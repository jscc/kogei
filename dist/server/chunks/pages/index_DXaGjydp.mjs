import { d as createAstro, e as createComponent, r as renderTemplate, m as maybeRenderHead, s as spreadAttributes, i as renderComponent, u as unescapeHTML, j as Fragment, g as addAttribute } from '../astro_DBWtJ5Wz.mjs';
import 'kleur/colors';
import { c as $$Image, d as $$Icon, e as $$Button, $ as $$WidgetWrapper, a as $$Headline, b as $$PageLayout } from './about_D31UocFq.mjs';
import { twMerge } from 'tailwind-merge';

const $$Astro$6 = createAstro("https:kogeistudio.com");
const $$HeroCustom = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$6, $$props, $$slots);
  Astro2.self = $$HeroCustom;
  const {
    id,
    title = await Astro2.slots.render("title"),
    subtitle = await Astro2.slots.render("subtitle"),
    tagline,
    content = await Astro2.slots.render("content"),
    actions = await Astro2.slots.render("actions"),
    image = await Astro2.slots.render("image")
  } = Astro2.props;
  return renderTemplate`${maybeRenderHead()}<section class="relative md:-mt-[76px] not-prose"${spreadAttributes(id ? { id } : {})}> <div class="absolute inset-0 pointer-events-none" aria-hidden="true"></div> <div class="relative max-w-7xl mx-auto px-4 sm:px-6 h-110vh"> <div class="pt-0 md:pt-[76px] pointer-events-none"></div> <div class="relative"> <div class="absolute z-10 inset-y-0 inset-x-0 flex items-center"> <span class="text-8xl font-bold drop-shadow-xl">
Empowering your ideas
</span> </div> <div> ${image && renderTemplate`<div class="relative m-auto max-w-full"> ${typeof image === "string" ? renderTemplate`${renderComponent($$result, "Fragment", Fragment, {}, { "default": ($$result2) => renderTemplate`${unescapeHTML(image)}` })}` : renderTemplate`${renderComponent($$result, "Image", $$Image, { "class": "mx-auto w-full h-screen", "widths": [400, 1768, 1624, 2040], "sizes": "(max-width: 767px) 400px, (max-width: 1023px) 1768px, (max-width: 2039px) 1624px, 2040px", "loading": "eager", "width": 1624, "height": 736, ...image })}`} </div>`} </div> </div> </div> </section>`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/widgets/HeroCustom.astro", void 0);

const $$Astro$5 = createAstro("https:kogeistudio.com");
const $$Note = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$5, $$props, $$slots);
  Astro2.self = $$Note;
  return renderTemplate`${maybeRenderHead()}<section class="bg-blue-50 dark:bg-slate-800 not-prose"> <div class="max-w-6xl mx-auto px-4 sm:px-6 py-4 text-md text-center font-medium"> <span class="font-bold"> ${renderComponent($$result, "Icon", $$Icon, { "name": "tabler:info-square", "class": "w-5 h-5 inline-block align-text-bottom" })} Our Philosophy:</span> Simplicity, Best Practices and High Performance
</div> </section>`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/widgets/Note.astro", void 0);

const $$Astro$4 = createAstro("https:kogeistudio.com");
const $$ItemGrid = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$4, $$props, $$slots);
  Astro2.self = $$ItemGrid;
  const { items = [], columns, defaultIcon = "", classes = {} } = Astro2.props;
  const {
    container: containerClass = "",
    panel: panelClass = "",
    title: titleClass = "",
    description: descriptionClass = "",
    icon: defaultIconClass = "text-primary",
    action: actionClass = ""
  } = classes;
  return renderTemplate`${items && renderTemplate`${maybeRenderHead()}<div${addAttribute(twMerge(
    `grid mx-auto gap-8 md:gap-y-12 ${columns === 4 ? "lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2" : columns === 3 ? "lg:grid-cols-3 sm:grid-cols-2" : columns === 2 ? "sm:grid-cols-2 " : ""}`,
    containerClass
  ), "class")}>${items.map(({ title, description, icon, callToAction, classes: itemClasses = {} }) => renderTemplate`<div><div${addAttribute(twMerge("flex flex-row max-w-md", panelClass, itemClasses?.panel), "class")}><div class="flex justify-center">${(icon || defaultIcon) && renderTemplate`${renderComponent($$result, "Icon", $$Icon, { "name": icon || defaultIcon, "class": twMerge("w-7 h-7 mr-2 rtl:mr-0 rtl:ml-2", defaultIconClass, itemClasses?.icon) })}`}</div><div class="mt-0.5">${title && renderTemplate`<h3${addAttribute(twMerge("text-xl font-bold", titleClass, itemClasses?.title), "class")}>${title}</h3>`}${description && renderTemplate`<p${addAttribute(twMerge(`${title ? "mt-3" : ""} text-muted`, descriptionClass, itemClasses?.description), "class")}>${unescapeHTML(description)}</p>`}${callToAction && renderTemplate`<div${addAttribute(twMerge(
    `${title || description ? "mt-3" : ""}`,
    actionClass,
    itemClasses?.actionClass
  ), "class")}>${renderComponent($$result, "Button", $$Button, { "variant": "link", ...callToAction })}</div>`}</div></div></div>`)}</div>`}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/ui/ItemGrid.astro", void 0);

const $$Astro$3 = createAstro("https:kogeistudio.com");
const $$Features = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$3, $$props, $$slots);
  Astro2.self = $$Features;
  const {
    title = await Astro2.slots.render("title"),
    subtitle = await Astro2.slots.render("subtitle"),
    tagline = await Astro2.slots.render("tagline"),
    items = [],
    columns = 2,
    defaultIcon,
    id,
    isDark = false,
    classes = {},
    bg = await Astro2.slots.render("bg")
  } = Astro2.props;
  return renderTemplate`${renderComponent($$result, "WidgetWrapper", $$WidgetWrapper, { "id": id, "isDark": isDark, "containerClass": `max-w-5xl ${classes?.container ?? ""}`, "bg": bg }, { "default": ($$result2) => renderTemplate` ${renderComponent($$result2, "Headline", $$Headline, { "title": title, "subtitle": subtitle, "tagline": tagline, "classes": classes?.headline })} ${renderComponent($$result2, "ItemGrid", $$ItemGrid, { "items": items, "columns": columns, "defaultIcon": defaultIcon, "classes": {
    container: "",
    title: "md:text-[1.3rem]",
    icon: "text-white bg-primary rounded-full w-10 h-10 p-2 md:w-12 md:h-12 md:p-3 mr-4 rtl:ml-4 rtl:mr-0",
    ...classes?.items ?? {}
  } })} ` })}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/widgets/Features.astro", void 0);

const $$Astro$2 = createAstro("https:kogeistudio.com");
const $$Timeline = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$2, $$props, $$slots);
  Astro2.self = $$Timeline;
  const { items = [], classes = {}, defaultIcon } = Astro2.props;
  const {
    container: containerClass = "",
    panel: panelClass = "",
    title: titleClass = "",
    description: descriptionClass = "",
    icon: defaultIconClass = "text-primary dark:text-slate-200 border-primary dark:border-blue-700"
  } = classes;
  return renderTemplate`${items && items.length && renderTemplate`${maybeRenderHead()}<div${addAttribute(containerClass, "class")}>${items.map(
    ({ title, description, icon, classes: itemClasses = {} }, index = 0) => renderTemplate`<div${addAttribute(twMerge("flex", panelClass, itemClasses?.panel), "class")}><div class="flex flex-col items-center mr-4 rtl:mr-0 rtl:ml-4"><div><div class="flex items-center justify-center">${(icon || defaultIcon) && renderTemplate`${renderComponent($$result, "Icon", $$Icon, { "name": icon || defaultIcon, "class": twMerge(
      "w-10 h-10 p-2 rounded-full border-2",
      defaultIconClass,
      itemClasses?.icon
    ) })}`}</div></div>${index !== items.length - 1 && renderTemplate`<div class="w-px h-full bg-black/10 dark:bg-slate-400/50"></div>`}</div><div${addAttribute(`pt-1 ${index !== items.length - 1 ? "pb-8" : ""}`, "class")}>${title && renderTemplate`<p${addAttribute(twMerge(
      "text-xl font-bold",
      titleClass,
      itemClasses?.title
    ), "class")}>${unescapeHTML(title)}</p>`}${description && renderTemplate`<p${addAttribute(twMerge(
      "text-muted mt-2",
      descriptionClass,
      itemClasses?.description
    ), "class")}>${unescapeHTML(description)}</p>`}</div></div>`
  )}</div>`}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/ui/Timeline.astro", void 0);

const $$Astro$1 = createAstro("https:kogeistudio.com");
const $$Steps = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$1, $$props, $$slots);
  Astro2.self = $$Steps;
  const {
    title = await Astro2.slots.render("title"),
    subtitle = await Astro2.slots.render("subtitle"),
    tagline = await Astro2.slots.render("tagline"),
    items = [],
    image = await Astro2.slots.render("image"),
    isReversed = false,
    id,
    isDark = false,
    classes = {},
    bg = await Astro2.slots.render("bg")
  } = Astro2.props;
  return renderTemplate`${renderComponent($$result, "WidgetWrapper", $$WidgetWrapper, { "id": id, "isDark": isDark, "containerClass": `max-w-5xl ${classes?.container ?? ""}`, "bg": bg }, { "default": ($$result2) => renderTemplate` ${maybeRenderHead()}<div${addAttribute(["flex flex-col gap-8 md:gap-12", { "md:flex-row-reverse": isReversed }, { "md:flex-row": image }], "class:list")}> <div${addAttribute(["md:py-4 md:self-center", { "md:basis-1/2": image }, { "w-full": !image }], "class:list")}> ${renderComponent($$result2, "Headline", $$Headline, { "title": title, "subtitle": subtitle, "tagline": tagline, "classes": {
    container: "text-left rtl:text-right",
    title: "text-3xl lg:text-4xl",
    ...classes?.headline ?? {}
  } })} ${renderComponent($$result2, "Timeline", $$Timeline, { "items": items, "classes": classes?.items })} </div> ${image && renderTemplate`<div class="relative md:basis-1/2"> ${typeof image === "string" ? renderTemplate`${renderComponent($$result2, "Fragment", Fragment, {}, { "default": ($$result3) => renderTemplate`${unescapeHTML(image)}` })}` : renderTemplate`${renderComponent($$result2, "Image", $$Image, { "class": "inset-0 object-cover object-top w-full rounded-md shadow-lg md:absolute md:h-full bg-gray-400 dark:bg-slate-700", "widths": [400, 768], "sizes": "(max-width: 768px) 100vw, 432px", "width": 432, "height": 768, "layout": "cover", "src": image?.src, "alt": image?.alt || "" })}`} </div>`} </div> ` })}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/widgets/Steps.astro", void 0);

const $$Astro = createAstro("https:kogeistudio.com");
const $$Index = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$Index;
  const metadata = {
    title: "K\u014Dgei Studio",
    ignoreTitleTemplate: true
  };
  return renderTemplate`${renderComponent($$result, "Layout", $$PageLayout, { "metadata": metadata }, { "default": ($$result2) => renderTemplate` ${renderComponent($$result2, "Hero", $$HeroCustom, { "image": { src: "~/assets/images/focus.jpg", alt: "Kogei" } })} ${renderComponent($$result2, "Note", $$Note, {})} ${renderComponent($$result2, "Features", $$Features, { "id": "features", "tagline": "Services", "title": "How we help you succeed", "items": [
    {
      title: "Web Development",
      description: "Design and build custom websites that are sleek, user-friendly, and optimized for success. Using the latest technologies and best practices to deliver high-quality, user-friendly websites that drive results.",
      icon: "tabler:timeline"
    },
    {
      title: "Backend and API Development",
      description: "Robust, scalable, and efficient backend solutions tailored to meet your specific business needs. We handle the technical complexities, allowing you to focus on growing your business.",
      icon: "tabler:settings-2"
    },
    {
      title: "Data Engineering",
      description: "Building scalable data pipelines, optimizing data workflows, and implementing advanced analytics solutions. Turn your raw data into actionable insights",
      icon: "tabler:chart-grid-dots"
    },
    {
      title: "Web Accessibility",
      description: "Training, Assessing, auditing, and enhancing the accessibility of your digital assets to ensure they are usable by all individuals, regardless of ability.",
      icon: "tabler:accessible"
    }
  ] })} ${renderComponent($$result2, "Steps", $$Steps, { "title": "Make your vision a reality.", "items": [
    {
      title: 'Step 1: <span class="font-medium">Tells us your vision</span>',
      description: "This step is all about you \u2013 telling us your vision. Share your ideas, goals, and aspirations with us, and let's turn your vision into reality. Together, let's create something remarkable.",
      icon: "tabler:telescope"
    },
    {
      title: 'Step 2: <span class="font-medium">Discuss details</em>',
      description: "We ensure that your requirements, preferences, objectives and expectations are well understood so that our solutions are precisely tailored to meet and exceed your expectations.",
      icon: "tabler:letter-case"
    },
    {
      title: 'Step 3: <span class="font-medium">Delivery</span>',
      description: "This is where your idea meets the real world. But you won't be alone! We'll provide support, expertise, and dedication to ensure your project's success.",
      icon: "tabler:packages"
    },
    {
      title: "Ready!",
      icon: "tabler:rocket"
    }
  ], "image": {
    src: "https://images.unsplash.com/photo-1616198814651-e71f960c3180?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=987&q=80",
    alt: "Steps image"
  } })} ` })}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/pages/index.astro", void 0);

const $$file = "/Users/juliocosta/Documents/projects/kogei/src/pages/index.astro";
const $$url = "";

export { $$Index as default, $$file as file, $$url as url };
