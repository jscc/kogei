import { e as createComponent, r as renderTemplate, i as renderComponent, u as unescapeHTML } from '../astro_DBWtJ5Wz.mjs';
import 'kleur/colors';
import { $ as $$MarkdownLayout } from './gdpr_linBXC8Z.mjs';

const html = "<p><em>Last updated</em>: April 02, 2024</p>\n<h2 id=\"privacy-policy-for-your-web-consultancy-name\"><strong>Privacy Policy for [Your Web Consultancy Name]</strong></h2>\n<p>[Your Web Consultancy Name] (“us”, “we”, or “our”) is committed to protecting the privacy of our clients and visitors to our website. This Privacy Policy outlines the types of personal information we collect, how it is used, and your choices regarding your data.</p>\n<h3 id=\"information-we-collect\"><strong>Information We Collect</strong></h3>\n<p>We may collect personal information from you when you visit our website, interact with us through contact forms or live chat, or when you engage our services. The types of personal information we may collect include:</p>\n<ul>\n<li>Name</li>\n<li>Email address</li>\n<li>Phone number</li>\n<li>Company name</li>\n<li>Website URL</li>\n<li>Other information you voluntarily provide to us</li>\n</ul>\n<h3 id=\"how-we-use-your-information\"><strong>How We Use Your Information</strong></h3>\n<p>We may use the information we collect for various purposes, including:</p>\n<ul>\n<li>Providing and maintaining our services</li>\n<li>Responding to your inquiries and requests</li>\n<li>Communicating with you about our services, promotions, and updates</li>\n<li>Analyzing and improving our website and services</li>\n<li>Complying with legal requirements</li>\n</ul>\n<h3 id=\"data-security\"><strong>Data Security</strong></h3>\n<p>We are committed to protecting the security of your personal information. We implement appropriate technical and organizational measures to safeguard your data against unauthorized access, alteration, disclosure, or destruction.</p>\n<h3 id=\"third-party-services\"><strong>Third-Party Services</strong></h3>\n<p>We may use third-party service providers to assist us in delivering our services or to help us analyze how our website is used. These third parties may have access to your personal information only to perform specific tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>\n<h3 id=\"your-choices\"><strong>Your Choices</strong></h3>\n<p>You have the right to access, update, or delete your personal information. If you would like to do so, please contact us using the information provided below.</p>\n<h3 id=\"changes-to-this-privacy-policy\"><strong>Changes to This Privacy Policy</strong></h3>\n<p>We may update our Privacy Policy from time to time. Any changes will be posted on this page, and the revised date will be updated below.</p>\n<p><strong>Contact Us</strong></p>\n<p>If you have any questions about this Privacy Policy or our practices regarding your personal information, please contact us at [contact email or address].</p>";

				const frontmatter = {"title":"Privacy Policy","layout":"~/layouts/MarkdownLayout.astro","readingTime":2};
				const file = "/Users/juliocosta/Documents/projects/kogei/src/pages/privacy.md";
				const url = "/privacy";
				function rawContent() {
					return "_Last updated_: April 02, 2024\n\n\n## **Privacy Policy for [Your Web Consultancy Name]**\n\n[Your Web Consultancy Name] (\"us\", \"we\", or \"our\") is committed to protecting the privacy of our clients and visitors to our website. This Privacy Policy outlines the types of personal information we collect, how it is used, and your choices regarding your data.\n\n### **Information We Collect**\n\nWe may collect personal information from you when you visit our website, interact with us through contact forms or live chat, or when you engage our services. The types of personal information we may collect include:\n\n* Name\n* Email address\n* Phone number\n* Company name\n* Website URL\n* Other information you voluntarily provide to us\n\n### **How We Use Your Information**\n\nWe may use the information we collect for various purposes, including:\n\n* Providing and maintaining our services\n* Responding to your inquiries and requests\n* Communicating with you about our services, promotions, and updates\n* Analyzing and improving our website and services\n* Complying with legal requirements\n\n### **Data Security**\n\nWe are committed to protecting the security of your personal information. We implement appropriate technical and organizational measures to safeguard your data against unauthorized access, alteration, disclosure, or destruction.\n\n### **Third-Party Services**\n\nWe may use third-party service providers to assist us in delivering our services or to help us analyze how our website is used. These third parties may have access to your personal information only to perform specific tasks on our behalf and are obligated not to disclose or use it for any other purpose.\n\n### **Your Choices**\n\nYou have the right to access, update, or delete your personal information. If you would like to do so, please contact us using the information provided below.\n\n### **Changes to This Privacy Policy**\n\nWe may update our Privacy Policy from time to time. Any changes will be posted on this page, and the revised date will be updated below.\n\n**Contact Us**\n\nIf you have any questions about this Privacy Policy or our practices regarding your personal information, please contact us at [contact email or address].\n";
				}
				function compiledContent() {
					return html;
				}
				function getHeadings() {
					return [{"depth":2,"slug":"privacy-policy-for-your-web-consultancy-name","text":"Privacy Policy for [Your Web Consultancy Name]"},{"depth":3,"slug":"information-we-collect","text":"Information We Collect"},{"depth":3,"slug":"how-we-use-your-information","text":"How We Use Your Information"},{"depth":3,"slug":"data-security","text":"Data Security"},{"depth":3,"slug":"third-party-services","text":"Third-Party Services"},{"depth":3,"slug":"your-choices","text":"Your Choices"},{"depth":3,"slug":"changes-to-this-privacy-policy","text":"Changes to This Privacy Policy"}];
				}

				const Content = createComponent((result, _props, slots) => {
					const { layout, ...content } = frontmatter;
					content.file = file;
					content.url = url;

					return renderTemplate`${renderComponent(result, 'Layout', $$MarkdownLayout, {
								file,
								url,
								content,
								frontmatter: content,
								headings: getHeadings(),
								rawContent,
								compiledContent,
								'server:root': true,
							}, {
								'default': () => renderTemplate`${unescapeHTML(html)}`
							})}`;
				});

export { Content, compiledContent, Content as default, file, frontmatter, getHeadings, rawContent, url };
