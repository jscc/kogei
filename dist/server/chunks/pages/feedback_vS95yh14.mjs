import sendGrid from '@sendgrid/mail';

sendGrid.setApiKey("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");
const POST = async ({ request }) => {
  const data = await request.formData();
  const name = data.get("name");
  const email = data.get("email");
  const message = data.get("message");
  console.log({
    name,
    email,
    message
  });
  if (!name || !email || !message) {
    return new Response(
      JSON.stringify({
        message: "Missing required fields"
      }),
      { status: 400 }
    );
  }
  const msg = {
    to: "contact@kogeistudio.com",
    // Change to your recipient
    from: "contact@kogeistudio.com",
    // Change to your verified sender
    replyTo: { email, name },
    subject: `Contact form submission from ${name}`,
    text: message
  };
  await sendGrid.send(msg).then(() => {
    console.log("Email sent");
  }).catch((error) => {
    console.error(error);
  });
  return new Response(
    JSON.stringify({
      message: "Success!"
    }),
    { status: 200 }
  );
};

export { POST };
