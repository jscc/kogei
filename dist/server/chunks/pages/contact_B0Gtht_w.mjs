import { d as createAstro, e as createComponent, r as renderTemplate, i as renderComponent, m as maybeRenderHead } from '../astro_DBWtJ5Wz.mjs';
import 'kleur/colors';
import { $ as $$WidgetWrapper, a as $$Headline, b as $$PageLayout } from './about_D31UocFq.mjs';
import sendGrid from '@sendgrid/mail';
import { jsxs, jsx } from 'react/jsx-runtime';
import { useState } from 'react';
/* empty css                            */

function Form() {
  const [responseMessage, setResponseMessage] = useState("");
  async function submit(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    const response = await fetch("/api/feedback", {
      method: "POST",
      body: formData
    });
    const data = await response.json();
    if (data.message) {
      setResponseMessage(data.message);
    }
  }
  return /* @__PURE__ */ jsxs("form", { onSubmit: submit, children: [
    /* @__PURE__ */ jsxs("label", { children: [
      "Name",
      /* @__PURE__ */ jsx("input", { type: "text", id: "name", name: "name", required: true })
    ] }),
    /* @__PURE__ */ jsxs("label", { children: [
      "Email",
      /* @__PURE__ */ jsx("input", { type: "email", id: "email", name: "email", required: true })
    ] }),
    /* @__PURE__ */ jsxs("label", { children: [
      "Message",
      /* @__PURE__ */ jsx("textarea", { id: "message", name: "message", required: true })
    ] }),
    /* @__PURE__ */ jsx("button", { children: "Send" }),
    responseMessage && /* @__PURE__ */ jsx("p", { children: responseMessage })
  ] });
}

const $$Astro$1 = createAstro("https:kogeistudio.com");
const $$Contact$1 = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro$1, $$props, $$slots);
  Astro2.self = $$Contact$1;
  sendGrid.setApiKey("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");
  const {
    title = await Astro2.slots.render("title"),
    subtitle = await Astro2.slots.render("subtitle"),
    tagline = await Astro2.slots.render("tagline"),
    id,
    isDark = false,
    classes = {},
    bg = await Astro2.slots.render("bg")
  } = Astro2.props;
  return renderTemplate`${renderComponent($$result, "WidgetWrapper", $$WidgetWrapper, { "id": id, "isDark": isDark, "containerClass": `max-w-7xl mx-auto ${classes?.container ?? ""}`, "bg": bg, "data-astro-cid-3enrl7v5": true }, { "default": ($$result2) => renderTemplate` ${renderComponent($$result2, "Headline", $$Headline, { "title": title, "subtitle": subtitle, "tagline": tagline, "data-astro-cid-3enrl7v5": true })} ${maybeRenderHead()}<div class="flex flex-col w-full backdrop-blur bg-white border border-gray-200 dark:bg-slate-900 dark:border-gray-700 lg:p-8 max-w-xl mx-auto p-4 rounded-lg shadow sm:p-6" id="mc_embed_shell" data-astro-cid-3enrl7v5> ${renderComponent($$result2, "FeedbackForm", Form, { "client:load": true, "client:component-hydration": "load", "client:component-path": "~/components/FeedbackForm", "client:component-export": "default", "data-astro-cid-3enrl7v5": true })} <!-- <div id="formContainer">
  <form method="post" action="/api/send">
    <div class="columns">
      <fieldset class="mb-6">
        <label class="block text-xs mb-2 font-medium" for="name">Name</label>
        <input class="w-full bg-white border border-gray-200 dark:bg-slate-900 dark:border-gray-700 rounded-lg block px-4 py-2 text-md"  type="text" id="name" value="" />
      </fieldset>
    </div>
    <fieldset class="mb-6 min-h-[90px]">
      <label class="block text-xs mb-2 font-medium" for="email">Email</label>
      <input class="w-full bg-white border border-gray-200 dark:bg-slate-900 dark:border-gray-700 rounded-lg block px-4 py-2 text-md"  type="email" id="email" value="" />
      <div id="emailError" class="text-red-500 text-xs mt-2 hidden">Please enter a valid email address.</div>
    </fieldset>
    <fieldset class="mb-6">
      <label class="block text-xs mb-2 font-medium" for="message">Message</label>
      <textarea class="w-full bg-white border border-gray-200 dark:bg-slate-900 dark:border-gray-700 rounded-lg block px-4 py-2 text-md"  id="message"></textarea>
    </fieldset>
    <div class="grid mt-10">
      <button class="btn-primary" type="submit">Submit</button>
    </div>
  </form>
</div>

  <div id="thankYouNotice" class="hidden">
    <p>Thank you for your contact!</p>
    <p>You'll be hearing from us shortly.</p>
  </div>
</div> --> </div>  ` })}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/components/widgets/Contact.astro", void 0);

const $$Astro = createAstro("https:kogeistudio.com");
const $$Contact = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$Contact;
  const metadata = {
    title: "Contact"
  };
  return renderTemplate`${renderComponent($$result, "Layout", $$PageLayout, { "metadata": metadata }, { "default": ($$result2) => renderTemplate` ${renderComponent($$result2, "ContactUs", $$Contact$1, { "title": "How can we help you?", "subtitle": "Tell us about your great ideas.", "inputs": [
    {
      type: "text",
      name: "name",
      label: "Name"
    },
    {
      type: "email",
      name: "email",
      label: "Email"
    }
  ], "textarea": {
    label: "Message"
  }, "disclaimer": {
    label: "By submitting this contact form, you acknowledge and agree to the collection of your personal information."
  } })} ` })}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/pages/contact.astro", void 0);

const $$file = "/Users/juliocosta/Documents/projects/kogei/src/pages/contact.astro";
const $$url = "/contact";

export { $$Contact as default, $$file as file, $$url as url };
