import { d as createAstro, e as createComponent, r as renderTemplate, i as renderComponent, m as maybeRenderHead, l as renderSlot, u as unescapeHTML } from '../astro_DBWtJ5Wz.mjs';
import 'kleur/colors';
import { b as $$PageLayout } from './about_D31UocFq.mjs';

const $$Astro = createAstro("https:kogeistudio.com");
const $$MarkdownLayout = createComponent(async ($$result, $$props, $$slots) => {
  const Astro2 = $$result.createAstro($$Astro, $$props, $$slots);
  Astro2.self = $$MarkdownLayout;
  const { frontmatter } = Astro2.props;
  const metadata = {
    title: frontmatter?.title
  };
  return renderTemplate`${renderComponent($$result, "Layout", $$PageLayout, { "metadata": metadata }, { "default": ($$result2) => renderTemplate` ${maybeRenderHead()}<section class="px-4 py-16 sm:px-6 mx-auto lg:px-8 lg:py-20 max-w-4xl"> <h1 class="font-bold font-heading text-4xl md:text-5xl leading-tighter tracking-tighter">${frontmatter.title}</h1> <div class="mx-auto prose prose-lg max-w-4xl dark:prose-invert dark:prose-headings:text-slate-300 prose-md prose-headings:font-heading prose-headings:leading-tighter prose-headings:tracking-tighter prose-headings:font-bold prose-a:text-blue-600 dark:prose-a:text-blue-400 prose-img:rounded-md prose-img:shadow-lg mt-8"> ${renderSlot($$result2, $$slots["default"])} </div> </section> ` })}`;
}, "/Users/juliocosta/Documents/projects/kogei/src/layouts/MarkdownLayout.astro", void 0);

const html = "<p><em>Last updated</em>: April 02, 2024</p>\n<p>[Your Company Name] (“us”, “we”, or “our”) is committed to protecting the privacy and security of your personal data. This GDPR Statement outlines how we collect, use, and protect your personal information in compliance with the General Data Protection Regulation (GDPR).</p>\n<h3 id=\"data-collection-and-use\"><strong>Data Collection and Use</strong></h3>\n<p>We may collect personal information from you when you interact with our website, subscribe to our newsletter, or engage with our services. The types of personal information we may collect include:</p>\n<ul>\n<li>Name</li>\n<li>Email address</li>\n<li>Phone number</li>\n<li>Other information you voluntarily provide to us</li>\n</ul>\n<p>We use this information to provide and improve our services, communicate with you, and personalize your experience on our website.</p>\n<h3 id=\"data-protection-and-security\"><strong>Data Protection and Security</strong></h3>\n<p>We implement appropriate technical and organizational measures to safeguard your personal data against unauthorized access, alteration, disclosure, or destruction. We regularly review and update our security measures to ensure the integrity and confidentiality of your information.</p>\n<h3 id=\"data-sharing-and-third-parties\"><strong>Data Sharing and Third Parties</strong></h3>\n<p>We may share your personal information with trusted third-party service providers to assist us in delivering our services or to help us analyze how our website is used. These third parties are contractually obligated to use your personal information only for the purposes specified by us and are required to maintain the confidentiality and security of your data.</p>\n<h3 id=\"your-rights\"><strong>Your Rights</strong></h3>\n<p>You have the right to access, update, or delete your personal information. If you would like to exercise any of these rights or have any questions about our GDPR compliance practices, please contact us using the information provided below.</p>\n<h3 id=\"changes-to-this-gdpr-statement\"><strong>Changes to this GDPR Statement</strong></h3>\n<p>We may update this GDPR Statement from time to time to reflect changes in our privacy practices or legal requirements. Any updates will be posted on this page, and the revised date will be updated below.</p>\n<h3 id=\"contact-us\"><strong>Contact Us</strong></h3>\n<p>If you have any questions or concerns about our GDPR compliance practices or this GDPR Statement, please contact us at [contact email or address].</p>";

				const frontmatter = {"title":"GDPR Statement","layout":"~/layouts/MarkdownLayout.astro","readingTime":2};
				const file = "/Users/juliocosta/Documents/projects/kogei/src/pages/gdpr.md";
				const url = "/gdpr";
				function rawContent() {
					return "_Last updated_: April 02, 2024\n\n[Your Company Name] (\"us\", \"we\", or \"our\") is committed to protecting the privacy and security of your personal data. This GDPR Statement outlines how we collect, use, and protect your personal information in compliance with the General Data Protection Regulation (GDPR).\n\n### **Data Collection and Use**\n\nWe may collect personal information from you when you interact with our website, subscribe to our newsletter, or engage with our services. The types of personal information we may collect include:\n\n* Name\n* Email address\n* Phone number\n* Other information you voluntarily provide to us\n\nWe use this information to provide and improve our services, communicate with you, and personalize your experience on our website.\n\n### **Data Protection and Security**\n\nWe implement appropriate technical and organizational measures to safeguard your personal data against unauthorized access, alteration, disclosure, or destruction. We regularly review and update our security measures to ensure the integrity and confidentiality of your information.\n\n### **Data Sharing and Third Parties**\n\nWe may share your personal information with trusted third-party service providers to assist us in delivering our services or to help us analyze how our website is used. These third parties are contractually obligated to use your personal information only for the purposes specified by us and are required to maintain the confidentiality and security of your data.\n\n### **Your Rights**\n\nYou have the right to access, update, or delete your personal information. If you would like to exercise any of these rights or have any questions about our GDPR compliance practices, please contact us using the information provided below.\n\n### **Changes to this GDPR Statement**\n\nWe may update this GDPR Statement from time to time to reflect changes in our privacy practices or legal requirements. Any updates will be posted on this page, and the revised date will be updated below.\n\n### **Contact Us**\n\nIf you have any questions or concerns about our GDPR compliance practices or this GDPR Statement, please contact us at [contact email or address].\n";
				}
				function compiledContent() {
					return html;
				}
				function getHeadings() {
					return [{"depth":3,"slug":"data-collection-and-use","text":"Data Collection and Use"},{"depth":3,"slug":"data-protection-and-security","text":"Data Protection and Security"},{"depth":3,"slug":"data-sharing-and-third-parties","text":"Data Sharing and Third Parties"},{"depth":3,"slug":"your-rights","text":"Your Rights"},{"depth":3,"slug":"changes-to-this-gdpr-statement","text":"Changes to this GDPR Statement"},{"depth":3,"slug":"contact-us","text":"Contact Us"}];
				}

				const Content = createComponent((result, _props, slots) => {
					const { layout, ...content } = frontmatter;
					content.file = file;
					content.url = url;

					return renderTemplate`${renderComponent(result, 'Layout', $$MarkdownLayout, {
								file,
								url,
								content,
								frontmatter: content,
								headings: getHeadings(),
								rawContent,
								compiledContent,
								'server:root': true,
							}, {
								'default': () => renderTemplate`${unescapeHTML(html)}`
							})}`;
				});

const gdpr = /*#__PURE__*/Object.freeze(/*#__PURE__*/Object.defineProperty({
  __proto__: null,
  Content,
  compiledContent,
  default: Content,
  file,
  frontmatter,
  getHeadings,
  rawContent,
  url
}, Symbol.toStringTag, { value: 'Module' }));

export { $$MarkdownLayout as $, gdpr as g };
