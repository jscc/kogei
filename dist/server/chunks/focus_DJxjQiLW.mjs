const focus = new Proxy({"src":"/_astro/focus.BRljU84k.jpg","width":3264,"height":4912,"format":"jpg"}, {
						get(target, name, receiver) {
							if (name === 'clone') {
								return structuredClone(target);
							}
							if (name === 'fsPath') {
								return "/Users/juliocosta/Documents/projects/kogei/src/assets/images/focus.jpg";
							}
							
							return target[name];
						}
					});

export { focus as default };
